# Visualisierung der ScrapeEuCalendarAPP

CSV Dateien mit den gesammelten Terminen soll auf einer Karte visualisiert werden

Die Position eines jeden EU Kommissionsmitglied soll auf einer Karte angezeigt werden

## TODO

    - Speicherort der Kalender.csv ändern, damit remote zugriff möglich
      - zumbeispiel NAS
    - Rechtsgrundlage für die Veröffentlichung der Termine?
      - Warum haben einige Kommissare keine Termine
    - Pandas kann nicht einfach auf Samsng isntalliert werden.
      - pip install pandas scheitert wegen der instsallation von numpy
        - numpy über "its-pointless" repo installieren
          - https://wiki.termux.com/wiki/Package_Management#its-pointless_.28live_the_dream.29

## Daten Scrapen

- Jahreszahl muss noch hinzugefügt werden
  - muss zum zeitpunkt des scrapings gemacht werden
  - liegt das gefundene Datum ohne Jahr vor oder nach dem aktuellen Datum
    - Wenn vorher, muss es sich um das folgende Jahr handeln
    - wenn nachher handelt es sich um das aktuelle jahr

## Daten einlesen

- csv einlesen

## Daten verändern

- Wochentage in der csv sind auch manchmal "von bis"
  - also: Thu-Fri
  - muss abgefangen werden
- Teilweise keine Orte angegeben
  - Was ist da los?
    - Meist handelt es sich bei diesen Terminen um Einladungen von den jeweiligen Kommissionsmitgliedern, Somit scheint das Treffen am Ort des jeweiligen Kommisionsbüros statt zu finden. ABer welches. Luxemburg oder Belgien???

## NTN

mögliche locales des Systems über bash:`locale -a` herausfinden

## Python Entwicklung

Dieses Projekt unterstuetzt kein Python 2 und setzt konsequent auf Python 3. Die nachfolgenden Schritte sollte in der angegeben Reihenfolge ausgefuehrt werden, um einen funktionierende Python-Umgebung zu erhalten mit der entwickelt werden kann.

### Virtuelle Umgebung

Python unterstuetzt virtuelle Umgebungen. Damit ist es moeglich komplett losgeloest von installierten Paketen im Betriebssytem eine eigene Umgebung zu schaffen in der definierte Versionen bestimmter Paket installiert sind. Python nennt diese virtuellen Umgebungen ``venv`. Seit Python 3 existiert in jeder Python-Installation dass gleichnamige Modul.

```shell
python -m venv <Gewuenschter Name>
```

### Abhaengigkeiten

```shell
pip install -r requirements.txt
```

### Testen (optional)

Dieser Schritt ist optionial. Es sind bisher noch keine Test vollstaendig implementiert.

Alle Tests ausfuehren

```shell
python -m unittest test/*py
```
