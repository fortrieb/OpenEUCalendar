from scraper import Scraper, Job
from scraper_commissioner import CommissionerScraper
from scraper_page_number import PageNumberScraper
from scraper_calendar import CalendarScraper
import requests
import copy
from bs4 import BeautifulSoup as bs

BASE_URL = "https://ec.europa.eu/commission/commissioners/2019-2024/calendar_en"


def request_website(url: str) -> bs:
    """Request website and create beautifulsoup instance

    :param url: URL to website
    :return: BeautifulSoup instance of website
    """
    if url:
        req = requests.get(url)
        return bs(req.text, features="html.parser")


def main():
    site = request_website(BASE_URL)

    scraperCom = CommissionerScraper(copy.copy(site))
    commissionerJob = Job(scraperCom, 100)
    commissioners = commissionerJob.execute()

    page = PageNumberScraper(copy.copy(site))
    pageJob = Job(page, 100)
    pages = pageJob.execute()

    pages.append(("", "Home"))

    events = []
    
    # TODO Threading
    for p in pages:
        calendar = CalendarScraper(None, url=BASE_URL + p[0])
        calJob = Job(calendar, 100)
        events.extend(calJob.execute(commissioners))

    print("Found events: {}".format(len(events)))


main()
