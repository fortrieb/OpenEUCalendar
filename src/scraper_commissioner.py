from scraper import Scraper, Commissioner


class CommissionerScraper(Scraper):
    """ Scrape commissioner names and ids from calendar sidebar at eu.europa.eu """

    def run(self, options):
        site = self.__url_text__()
        options = site.select(
            "select#edit-field-editorial-section-multiple-tid > option")
        commissioners = []
        if options:
            for op in options:
                value = op['value']
                # TODO split in first and last name
                name = op.getText().strip()
                if value not in ('All'):
                    self.log.debug("ID: '{}'\tName: '{}'".format(value, name))
                    commissioners.append(Commissioner(name, int(value)))
        else:
            self.log.error("No options found in %s", self.job.url)
        return commissioners
