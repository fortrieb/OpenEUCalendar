from scraper import Scraper

class PageNumberScraper(Scraper):
    """Extract Page numbers from URL"""
    def run(self, options):
        site = self.__url_text__()
        elements = site.select(".pager__wrapper > ul.pager > li.pager__item a")
        pages = []
        if elements:
            for e in elements:
                page = e['href'].strip()
                title = e.getText().strip()
                # Check if title is number
                if title.isdigit():
                    pages.append((page, title))
                    self.log.debug("Found Page\t %s\t%s", page, title)
                else:
                    self.log.debug("Non digit title: %s", title)
        else:
            self.log.error("Can't extract page numbers, nothing found in %s", self.url)
        return pages