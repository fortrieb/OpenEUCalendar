from scraper import Scraper, Job, Commissioner, CalendarEntry
from bs4 import BeautifulSoup as bs
from datetime import date, datetime
from hashlib import sha256


class CalendarScraper(Scraper):

    def run(self, commissioners: list):
        """ 
        :param: commissioners list of found commissioners
        :return: list of calendar entrys
        """
        if not commissioners:
            raise AttributeError("Need commisioner as argument")
        site = self.__url_text__()

        entries = []
        elements = site.select(
            ".view-content .listing--agenda > li.listing__item > .node")
        self.log.debug("Elements found:\t%s", len(elements))
        for e in elements:
            # self.log.debug(e.find("div", class_="date-block"))
            event_date = self.__extract_date__(
                e.find("div", class_="date-block"))
            entry_com = self.__extract_commissionor__(
                e.find("div", class_="listing__author"), commissioners)
            location = self.__extract_location__(
                e.find("span", class_="meta__item"))
            title = e.find("h3", class_="listing__title").getText()
            category = self.__extract_category__(
                e.find("span", "meta__item meta__item--type"))
            entry_hash = self.__hash_calendar_entry__(
                title, event_date[0], event_date[1], entry_com)

            self.log.debug("Event date: %s", event_date)

            event = CalendarEntry(date_start=event_date[0], date_end=event_date[1],
                                  commissioner=entry_com, title=title, city=location[0], country=location[1], created=date.today(), updated=date.today(), category=category, entry_hash=entry_hash)
            self.log.info("Event found: %s", event)
            entries.append(event)
        return entries

    def __extract_location__(self, html: bs) -> str:
        if html:
            city = html.find("span", class_="locality")
            country = html.find("span", class_="country")
            found_city = None
            found_country = None
            if city:
                found_city = city.getText()
            if country:
                found_country = country.getText()
            return (found_city, found_country)
        else:
            self.log.info("No location provided!")
            return (None, None)

    def __extract_category__(self, html: bs) -> str:
        if html:
            return html.getText()

    def __extract_commissionor__(self, html: bs, commissioners: list) -> [Commissioner]:
        if html:
            author = html.getText().strip()
            # mutiple commissioners for single event
            author_sp = author.split(",")
            found_commissioner = []
            for a in author_sp:
                found_commissioner.append(self.__get_commissioner__(a, commissioners))
                self.log.debug("Commissioner found: %s", found_commissioner)
            return found_commissioner
        else:
            self.log.debug("Commissioner extraction needs input html elements")

    def __extract_date__(self, html: bs) -> date:
        """ Extract date and date ranges from calnedar list

        :param html: BS4 PageElement to parse
        :return: date or date range as tuple
        """
        if html:
            day = html.find("span", class_="date-block__day").getText()
            month_elem = html.find(
                "span", class_="date-block__month").getText().strip()
            # TODO Year not provided in calendar entry
            year_now = date.today().year
            self.log.debug("extracted date: %s %s", day, month_elem)
            if self.__is_range__(day):
                self.log.debug("is range date: %s %s", day, month_elem)
                day_sp = day.split("-")
                month_sp = month_elem.split("-")
                # Each should be 2 elements if month and day are ranges
                if len(day_sp) + len(month_sp) == 4:
                    start_date = self.__str_to_date__(
                        year_now, month_sp[0], day_sp[0])
                    end_date = self.__str_to_date__(
                        year_now, month_sp[1], day_sp[1])
                    return (start_date, end_date)
                elif len(day_sp) + len(month_sp) == 3:
                    start_date = self.__str_to_date__(
                        year_now, month_sp[0], day_sp[0])
                    end_date = self.__str_to_date__(
                        year_now, month_sp[0], day_sp[1])
                    return (start_date, end_date)
                else:
                    self.log.error(
                        "Not supported date and month formate: %s, %s", day, month_elem)
            else:
                found_date = self.__str_to_date__(year_now, month_elem, day)
                return (found_date, found_date)
        else:
            self.log.error("Can't get event date. HTML is missing")

    def __is_range__(self, to_parse: str) -> bool:
        return len(to_parse.strip().split("-")) > 1

    def __str_to_date__(self, year: int, month: str, day: int) -> date:
        return datetime.strptime(str(year)+"-"+month+"-"+day+" 23:23:23", "%Y-%b-%d %H:%M:%S").date()

    def __get_commissioner__(self, name: str, all: list) -> Commissioner:
        """
        Search for commissioner in all found commissioners list

        :param name: Current name of commissioner
        :param commissioners: List of all commissioners
        :return: Found commissioner
        """
        for c in all:
            if name.lower().strip() == c.name.lower():
                return c

    def __hash_calendar_entry__(self, title: str, start: date, end: date, comissioners: [Commissioner]) -> str:
        com = ""
        for c in comissioners:
            com += "," + c.name
        return sha256((title + start.isoformat() + end.isoformat() + com).encode("utf-8")).hexdigest()
