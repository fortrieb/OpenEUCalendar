import requests
import logging
from abc import ABC, abstractmethod
from datetime import datetime, date
from dataclasses import dataclass
from bs4 import BeautifulSoup as bs

logging.basicConfig(level=logging.DEBUG)


@dataclass
class Commissioner:
    name: str
    id: int


@dataclass
class CalendarEntry():
    date_start: date
    date_end: date
    city: str
    country: str
    category: str
    title: str
    commissioner: [Commissioner]
    created: datetime
    updated: datetime
    entry_hash: str  # hash() is python build-in object function


class Scraper(ABC):
    """ Abstract scraper class provide usefull methods for scraping websites """

    def __init__(self, beautifulsoup: bs, url=None):
        self.url = url
        self.bs = beautifulsoup
        self.log = logging.getLogger("Scraper")

    @abstractmethod
    def run(self, options):
        self.log.debug("Scraper")
        raise NotImplementedError

    def __url_text__(self) -> bs:
        """ Requests Job URL and render site with BeautifulSoup

        :return: BeautifulSoup
        """
        if self.url:
            site = requests.get(self.url)
            return bs(site.text, features="html.parser")
        if self.bs:
            return self.bs
        raise AttributeError


class Job:
    def __init__(self, scraper: Scraper, intervall: int):
        """Job executes a scraper job in intervalls

        :param scraper: scraper to execute
        :param intervall: Intervall in seconds to run scraper
        """
        self.scraper = scraper
        self.intervall = intervall
        self.log = logging.getLogger("Job")

    def execute(self, options=None) -> list:
        """ Execute defined scraper durring initialization 

        :param options: param is optional, will be given to scraper run method
        :return list: found element
        """
        if self.scraper:
            return self.scraper.run(options)
