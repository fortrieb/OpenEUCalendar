import unittest
from src.scraper_page_number import PageNumberScraper

class ScraperTest(unittest.TestCase):
    def setUp(self):
        self.scraper = PageNumberScraper(None)

    @unittest.expectedFailure
    def test_error(self):
        self.scraper.run(None)